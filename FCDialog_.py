# -*- coding: utf-8 -*-
'''
*
*    FCDialog.py
*
*    A FreeCAD module that provides a simple input form as a base class.  Maintains
*    a Dialog with State.
*
*    Copyright 2019 Floating Cameras, LLC - Author: Marc Cole
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*

'''

from PySide import QtCore, QtGui
import FreeCAD, FreeCADGui, traceback, Part, Draft
import Mesh, pickle


import sys, os
from os.path import expanduser, isfile, isdir


class DialogItem:
    def __init__(self, po_field, ps_default):
        self.name = po_field.name
        self.label = po_field.label
        self.field_type = po_field.type
        self.default = ps_default
        self.flags = po_field.flags

        self.max = po_field.max
        self.min = po_field.min

        self.qlabel = None
        self.qfield = None

class DFieldDef :
    def __init__(self, ps_name, ps_label, ps_type, ps_default, pi_flags = 0, pf_min=None, pf_max=None):
        self.name = ps_name
        self.label = ps_label
        self.type = ps_type
        self.default = ps_default
        self.flags = pi_flags
        self.min = pf_min
        self.max = pf_max

class Ui_Dialog(object):
    def __init__(self, ps_name, pl_fields, po_App, po_Gui, pl_winsize):
        self.name = ps_name
        self.fields_ = pl_fields
        self.app = po_App
        self.gui = po_Gui
        self.width = pl_winsize[0]
        self.height = pl_winsize[1]

        # Responses are held as strings in a list for saving and restoring State
        self.responses = {}
        for field_ in self.fields_ :
            if field_ :
                self.responses[field_.name] = field_.default

        state_file = "{}\\FreeCAD\\Macro\\{}.state".format(os.getenv("APPDATA"), self.name)
        # If a State file exists open and update the default values in responses
        if isfile(state_file):
            with open(state_file, 'rb') as f:
                # The protocol version used is detected automatically, so we do not
                # have to specify it.
                statevals = pickle.load(f)
                for key in statevals.iterkeys() :
                    if key in self.responses :
                        self.responses[key] = statevals[key]

        # State has been 'restored'.  Set the default values for input
        self.fields = []
        for field in self.fields_ :
            if field :
                self.fields.append(DialogItem(po_field = field, ps_default=self.responses[field.name]))

    def getValue(self, ps_field_name):
        for i in range(0, len(self.fields)) :
            field = self.fields[i]
            if field.name == ps_field_name :
                if field.field_type == "Text" :
                    return field.qfield.toPlainText()
                elif field.field_type == "Line" :
                    return field.qfield.text()
                elif field.field_type == "Input" :
                    return field.qfield.property("quantity")
                elif field.field_type == "CheckBox" :
                    return field.qfield.isChecked()

        return None

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(self.width+100, self.height + 35)
        Dialog.setWindowTitle(QtGui.QApplication.translate \
                                  ("Dialog", Dialog.ui.name, \
                                   None, QtGui.QApplication.UnicodeUTF8))
        self.dia = Dialog
        self.gridLayoutWidget = QtGui.QWidget(Dialog)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(19, 19, self.width, self.height))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")

        for i in range(0,len(self.fields)) :
            field = self.fields[i]

            field.qlabel = QtGui.QLabel(self.gridLayoutWidget)
            field.qlabel.setObjectName("label_".format(i))
            field.qlabel.setText(QtGui.QApplication.translate \
                               ("Dialog", field.label, None, QtGui.QApplication.UnicodeUTF8))

            self.gridLayout.addWidget(field.qlabel, i, 0, 1,1)
            if field.field_type == "Text" :
                field.qfield = QtGui.QTextEdit(self.gridLayoutWidget)

            elif field.field_type == "Line" :
                field.qfield = QtGui.QLineEdit(self.gridLayoutWidget)

            elif field.field_type == "Input" :
                fui = FreeCADGui.UiLoader()
                field.qfield = fui.createWidget("Gui::InputField")
                # if field.min :
                #     field.qfield.setMinimum(field.min)
                # if field.max :
                #     field.qfield.setMaximum(field.max)

            elif field.field_type == "CheckBox" :
                field.qfield = QtGui.QCheckBox(self.gridLayoutWidget)
                # field.qfield.setText(field.label)


            if field.qfield :
                field.qfield.setObjectName("field_".format(i))     # Apply friendly name to qfield
                self.gridLayout.addWidget(field.qfield,i,1,1,1)    # Add field to a specific cell
                if field.default :
                    if field.field_type == "CheckBox" :
                        field.qfield.setChecked(True if field.default == "True" else False)
                    else:
                        field.qfield.setText(field.default)

        self.buttonBox = QtGui.QDialogButtonBox(self.gridLayoutWidget)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons \
            (QtGui.QDialogButtonBox.Cancel | QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, len(self.fields), 1, 1, 1)

        QtCore.QObject.connect(self.buttonBox, \
                               QtCore.SIGNAL("accepted()"), self.makeSomething)
        QtCore.QObject.connect(self.buttonBox, \
                               QtCore.SIGNAL("rejected()"), self.makeNothing)
        QtCore.QMetaObject.connectSlotsByName(Dialog)


    def makeSomething(self):
        if self.validate_responses()  :
            self.dia.close()
            self.makeIt()
            self.app.ActiveDocument.recompute()

            # self.export(docName="override makeSomething()" )

            self.saveState()

            self.gui.SendMsgToActiveView("ViewFit")
            self.app.ActiveDocument.recompute()

        else :
            print "validation failed"

    def makeNothing(self):
        print "rejected!!"
        self.dia.close()

    def makeIt(self):
        print "Override makeIt() to use this Class"

    def validate_responses(self):
        '''
        Moves responses from Gui to responses dictionary. This is where
        response validation can be implemented.

        :return: True when all responses are valid, False if user needs to correct a value
        '''
        is_valid = True

        for i in range(0, len(self.fields)) :
            field = self.fields[i]
            if field.flags == 0 :
                self.responses[field.name] = str(self.getValue(field.name))
            elif field.flags == 1 :
                rcheck = self.getValue(field.name)
                if rcheck != field.default and rcheck.strip() != "" and isfile(rcheck) :
                    self.responses[field.name] = rcheck
            elif field.flags == 2 :
                rcheck = self.getValue(field.name)
                if rcheck != field.default and rcheck.strip() != "" and isdir(rcheck) :
                    self.responses[field.name] = rcheck
            else :
                self.responses[field.name] = str(self.getValue(field.name))


        return is_valid

    # ----------
    @staticmethod
    def say(s):
        FreeCAD.Console.PrintMessage(str(s) + "\n")

    @staticmethod
    def sayexc(mess=''):
        exc_type, exc_value, exc_traceback = sys.exc_info()
        ttt = repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
        lls = eval(ttt)
        FreeCAD.Console.PrintError(mess + "\n" + "-->  ".join(lls))

    @staticmethod
    def close_if_open(ps_docName):
        #  Cleanup -- Close project if it already is active to rebuild it
        if ps_docName in FreeCAD.listDocuments():
            FreeCAD.closeDocument(ps_docName)  # Close the application if opened

    def saveState(self):
        state_file = "{}\\FreeCAD\\Macro\\{}.state".format(os.getenv("APPDATA"), self.name)
        print ("Saving state to: {}".format(state_file))
        # state = pickle.dumps(responses)
        with open(state_file, 'wb') as f:
            # Pickle the 'data' dictionary using the highest protocol available.
            pickle.dump(self.responses, f, pickle.HIGHEST_PROTOCOL)

def ShowDialog(po_DialogDef):
    try:
        d = QtGui.QWidget()
        d.ui = po_DialogDef
        d.ui.setupUi(d)
        # d.adjustSize()
        d.show()

    except Exception:
        print Exception
        Ui_Dialog.sayexc(str(Exception.message))





